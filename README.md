# GoldenShoe

This is the website developed for GoldenShoe to improve the functionality of the existing GoldenShoe site.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

All required technologies can be found in the requirments.txt, to install the requirments in a virtualenv run

```
pip install -r requirments.txt
```

## Deployment

To deploy this clone the repo, cd into goldenshoe and run 
```
python manage.py migrate
```
then
```
python manage.py runserver
```
## Built With

* [Django](https://docs.djangoproject.com/en/3.0/) - The web framework used


## Authors

* [**Connor Kerrigan**] (https://gitlab.com/kerrigan)

