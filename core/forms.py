from django import forms
from django_countries.fields import CountryField


class CheckoutForm(forms.Form):
    street_address = forms.CharField()
    apartment_address = forms.CharField(required=False)
    country = CountryField(blank_label='Select Country').formfield(attrs={
        'class': 'custom-select d-block w-100'
    })
    post_code = forms.CharField()
    card_name = forms.CharField()
    card_number = forms.CharField()
    card_expiration = forms.CharField()
    card_cvv = forms.CharField()


class CouponForm(forms.Form):
    code = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Promo code',
        'aria-label': 'Recipient\'s username',
        'aria-describedby': 'basic-addon2'
    }))
