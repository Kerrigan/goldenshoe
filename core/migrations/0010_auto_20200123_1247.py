# Generated by Django 2.2.8 on 2020-01-23 12:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20200122_1900'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='item',
            options={'ordering': ['-release_date']},
        ),
        migrations.AddField(
            model_name='item',
            name='release_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
