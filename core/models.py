from django.db import models
from django.conf import settings
from django_countries.fields import CountryField
from django.shortcuts import reverse

CATEGORY_CHOICES = (
    ('MS', 'Men\'s Sportwear'),
    ('MEW', 'Men\'s Evening Wear'),
    ('MC', 'Men\'s Casual'),
    ('WS', 'Women\'s Sportwear'),
    ('WEW', 'Women\'s Evening Wear'),
    ('WC', 'Women\'s Casual'),
    ('O', 'Other')
)

LABEL_CHOICES = (
    ('D', 'danger'),
    ('P', 'primary'),
    ('S', 'secondary'),
    ('I', 'info')
)

STATUS_CHOICES = (
    ('P', 'Pending'),
    ('O', 'Ordered'),
    ('D', 'Delivered'),
    ('R', 'Refunded')
)


class Item(models.Model):
    title = models.CharField(max_length=100)
    price = models.FloatField()
    discount_price = models.FloatField(blank=True, null=True)
    category = models.CharField(choices=CATEGORY_CHOICES, max_length=3)
    label = models.CharField(choices=LABEL_CHOICES,
                             max_length=1, blank=True, null=True)
    slug = models.SlugField()
    description = models.TextField()
    release_date = models.DateTimeField()
    brief_desc = models.CharField(max_length=50, blank=True, null=True)
    image = models.ImageField()
    stock = models.IntegerField()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("core:product", kwargs={
            'slug': self.slug
        })

    def get_add_to_cart_url(self):
        return reverse("core:add-to-cart", kwargs={
            'slug': self.slug
        })

    def get_remove_from_cart_url(self):
        return reverse("core:remove-from-cart", kwargs={
            'slug': self.slug
        })

    class Meta:
        ordering = ['-release_date']


class OrderItem(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    ordered = models.BooleanField(default=False)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)

    def __str__(self):
        return f"{self.quantity} of {self.item.title}"

    def get_total_item_price(self):
        return self.quantity * self.item.price

    def get_total_item_discount_price(self):
        return self.quantity * self.item.discount_price

    def get_amount_saved(self):
        return self.get_total_item_price() - self.get_total_item_discount_price()

    def get_total_price(self):
        if self.item.discount_price:
            return self.get_total_item_discount_price()
        else:
            return self.get_total_item_price()


class Order(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    items = models.ManyToManyField(OrderItem)
    start_date = models.DateTimeField(auto_now_add=True)
    ordered_date = models.DateTimeField()
    ordered = models.BooleanField(default=False)
    billing_address = models.ForeignKey(
        'BillingAddress', on_delete=models.SET_NULL, blank=True, null=True)
    coupon = models.ForeignKey(
        'Coupon', on_delete=models.SET_NULL, blank=True, null=True)
    status = models.CharField(choices=STATUS_CHOICES,
                              max_length=1, blank=True, null=True)

    def __str__(self):
        return self.user.username

    def get_total(self):
        total = 0
        for order_item in self.items.all():
            total += order_item.get_total_price()
        if self.coupon:
            total -= self.coupon.value
        return total

    def get_total_items(self):
        total = 0
        for order_item in self.items.all():
            total += order_item.quantity
        return total


class BillingAddress(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    street_address = models.CharField(max_length=100)
    apartment_address = models.CharField(max_length=100)
    country = CountryField(multiple=False)
    post_code = models.CharField(max_length=15)

    def __str__(self):
        return self.user.username


class Coupon(models.Model):
    code = models.CharField(max_length=15)
    value = models.FloatField()

    def __str__(self):
        return self.code
