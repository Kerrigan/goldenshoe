from django import template
from core.models import Order

register = template.Library()


@register.filter
def cart_item_count(user):
    if user.is_authenticated:
        qs = Order.objects.filter(user=user, ordered=False)
        if qs.exists():
            order = qs[0]
            count = order.get_total_items()
            price = order.get_total()
            info = f"{count}"
            return info

    return 0
