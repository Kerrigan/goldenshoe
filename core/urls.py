from django.urls import path
from .views import home, product, ItemDetailView, add_to_cart, remove_from_cart, ProductView, OrderSummaryView, remove_single_from_cart, add_single_to_cart, checkingout, CheckOutView, support, add_coupon, account_orders

app_name = 'core'

urlpatterns = [
    path('', home, name='home'),
    path('checkout/', CheckOutView.as_view(), name='checkout'),
    path('order-summary/', OrderSummaryView.as_view(), name='order-summary'),
    path('product/', ProductView.as_view(), name='catalogue'),
    path('product/<slug>/', ItemDetailView.as_view(), name='product'),
    path('add-to-cart/<slug>', add_to_cart, name='add-to-cart'),
    path('remove-from-cart/<slug>', remove_from_cart, name='remove-from-cart'),
    path('remove-single-from-cart/<slug>',
         remove_single_from_cart, name='remove-single-from-cart'),
    path('add-single-from-cart/<slug>',
         add_single_to_cart, name='add-single-to-cart'),
    path('checking-out/', checkingout, name='checking-out'),
    path('support', support, name='support'),
    path('add-coupon/', add_coupon, name='add-coupon'),
    path('account/orders/', account_orders, name='account-orders'),
]
