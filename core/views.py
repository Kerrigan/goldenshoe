from django.shortcuts import render, get_object_or_404
from .models import Item, Order, OrderItem, BillingAddress, Coupon
from django.views.generic import ListView, DetailView, View
from django.shortcuts import redirect
from django.utils import timezone
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import CheckoutForm, CouponForm


def home(request):

    context = {
        'current': 'home'
    }
    return render(request, "home-page.html", context)


class CheckOutView(View):
    def get(self, *args, **kwargs):
        order = Order.objects.get(user=self.request.user, ordered=False)
        form = CheckoutForm()
        context = {
            'form': form,
            'order': order,
            'couponform': CouponForm()
        }
        return render(self.request, "checkout-page.html", context)

    def post(self, *args, **kwargs):
        form = CheckoutForm(self.request.POST or None)
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            if form.is_valid():
                street_address = form.cleaned_data.get('street_address')
                apartment_address = form.cleaned_data.get('apartment_address')
                country = form.cleaned_data.get('country')
                post_code = form.cleaned_data.get('post_code')
                card_name = form.cleaned_data.get('card_name')
                card_number = form.cleaned_data.get('card_number')
                card_expiration = form.cleaned_data.get('card_expiration')
                card_cvv = form.cleaned_data.get('card_cvv')

                billingaddress = BillingAddress(
                    user=self.request.user,
                    street_address=street_address,
                    apartment_address=apartment_address,
                    country=country,
                    post_code=post_code
                )
                billingaddress.save()
                order.billing_address = billingaddress
                order.save()
                return redirect('core:checking-out')

            messages.warning(self.request, "Failed to checkout")
            return redirect('core:checkout')

        except ObjectDoesNotExist:
            messages.error(request, "You do not have an active order!")
            return redirect("core:order-summary")


def product(request):
    context = {
        'current': 'products'
    }
    return render(request, "product-page.html", context)


class ItemDetailView(DetailView):
    model = Item
    template_name = "product-page.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        items = Item.objects.all().order_by('-stock')
        date = timezone.now()
        if self.request.user.is_authenticated:
            order = Order.objects.get_or_create(
                user=self.request.user, ordered=False)
            context['order'] = order[0]
        previous_page = self.request.META['HTTP_REFERER']
        context['additional_items'] = items[:3]
        context['previous_page'] = previous_page
        return context


@login_required
def add_to_cart(request, slug):
    item = get_object_or_404(Item, slug=slug)
    order_item, created = OrderItem.objects.get_or_create(
        item=item, user=request.user, ordered=False)
    order_queryset = Order.objects.filter(user=request.user, ordered=False)
    if order_queryset.exists():
        order = order_queryset[0]
        if order.items.filter(item__slug=item.slug).exists():
            message = f"{order_item.item.title} quantity was updated."
            messages.info(request, message)
            order_item.quantity += 1
            order_item.item.stock -= 1
            order_item.item.save()
            order_item.save()
        else:
            message = f"{order_item.item.title} were added to your cart."
            messages.info(request, message)
            order_item.item.stock -= 1
            order_item.item.save()
            order.items.add(order_item)
    else:
        date = timezone.now()
        order = Order.objects.create(user=request.user, ordered_date=date)
        order_item.item.stock -= 1
        order_item.item.save()
        order.items.add(order_item)
        message = f"{order_item.item.title} were added to your cart."
        messages.info(request, message)

    return redirect("core:product", slug=slug)


@login_required
def add_single_to_cart(request, slug):
    item = get_object_or_404(Item, slug=slug)
    order_item, created = OrderItem.objects.get_or_create(
        item=item, user=request.user, ordered=False)
    order_queryset = Order.objects.filter(user=request.user, ordered=False)
    if order_queryset.exists():
        order = order_queryset[0]
        if order.items.filter(item__slug=item.slug).exists():
            message = f"{order_item.item.title} quantity was updated."
            messages.info(request, message)
            order_item.quantity += 1
            order_item.item.stock -= 1
            order_item.item.save()
            order_item.save()
        else:
            message = f"{order_item.item.title} were added to your cart."
            messages.info(request, message)
            order_item.item.stock -= 1
            order_item.item.save()
            order.items.add(order_item)
    else:
        date = timezone.now()
        order = Order.objects.create(user=request.user, ordered_date=date)
        order_item.item.stock -= 1
        order_item.item.save()
        order.items.add(order_item)
        message = f"{order_item.item.title} were added to your cart."
        messages.info(request, message)

    return redirect("core:order-summary")


@login_required
def remove_from_cart(request, slug):
    item = get_object_or_404(Item, slug=slug)

    order_queryset = Order.objects.filter(user=request.user, ordered=False)
    if order_queryset.exists():
        order = order_queryset[0]
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item, user=request.user, ordered=False)[0]
            message = f"{order_item.item.title} were removed from your cart."
            order_item.item.stock += order_item.quantity
            order_item.item.save()
            order.items.remove(order_item)
            order_item.delete()
            messages.warning(request, message)
            return redirect("core:order-summary")
        else:
            message = f"{order_item.item.title} were not found in your cart."
            messages.info(request, message)
            return redirect("core:product", slug=slug)
    else:
        messages.danger(request, "You have nothing in your cart.")
        return redirect("core:product", slug=slug)


@login_required
def remove_single_from_cart(request, slug):
    item = get_object_or_404(Item, slug=slug)

    order_queryset = Order.objects.filter(user=request.user, ordered=False)
    if order_queryset.exists():
        order = order_queryset[0]
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item, user=request.user, ordered=False)[0]

            if order_item.quantity <= 1:
                order_item.item.stock += 1
                order_item.item.save()
                order_item.delete()
            else:
                order_item.item.stock += 1
                order_item.quantity -= 1
                order_item.item.save()
                order_item.save()

            message = f"{order_item.item.title} quantity was updated."
            messages.warning(request, message)
            return redirect("core:order-summary")
        else:

            message = f"{order_item.item.title} were not found in your cart."
            messages.info(request, message)
            return redirect("core:product", slug=slug)
    else:
        messages.danger(request, "You have nothing in your cart.")
        return redirect("core:product", slug=slug)


class ProductView(ListView):
    model = Item
    paginate_by = 12
    template_name = "catalogue-page.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current'] = 'products'
        return context


class OrderSummaryView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': order
            }
            return render(self.request, 'order-summary.html', context)
        except ObjectDoesNotExist:
            date = timezone.now()
            Order.objects.create(user=self.request.user, ordered_date=date)
            order = Order.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': order
            }
            return render(self.request, 'order-summary.html', context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current'] = 'cart'
        return context


def checkingout(request):
    user = request.user
    order = Order.objects.get(user=user, ordered=False)
    for item in order.items.all():
        item.ordered = True
        item.save()
    date = timezone.now()
    order.ordered = True
    order.ordered_date = date
    order.status = 'Ordered'
    order.save()
    message = f"{order.get_total_items()} item(s) ordered for £{order.get_total():.2f}, thank you for your order!"
    messages.info(request, message)
    Order.objects.create(user=user, ordered_date=date)
    return redirect("core:home")


def support(request):
    context = {
        'current': 'support'
    }
    return render(request, 'support.html', context)


def get_coupon(request, code):
    try:
        coupon = Coupon.objects.get(code=code)
        return coupon

    except ObjectDoesNotExist:
        messages.info(request, "Invalid Coupon!")
        return redirect("core:checkout")


def add_coupon(request):
    if request.method == "POST":
        form = CouponForm(request.POST or None)
        if form.is_valid():

            try:
                code = form.cleaned_data.get('code')
                order = Order.objects.get(user=request.user, ordered=False)

                try:
                    coupon = Coupon.objects.get(code=code)
                    order.coupon = coupon
                    order.save()
                    messages.success(request, "Successfully redeemed Coupon!")
                    return redirect("core:checkout")

                except ObjectDoesNotExist:
                    messages.info(request, "Invalid Coupon!")
                    return redirect("core:checkout")

            except ObjectDoesNotExist:
                messages.info(request, "You do not have an active order!")
                return redirect("core:checkout")
    return None


def account_orders(request):
    orders = Order.objects.filter(user=request.user, ordered=True)
    context = {
        'account_orders': orders
    }
    return render(request, 'account-orders.html', context)
